#You will need to install/update register the aks-preview extension. To do this you can use the following commands and make sure you have the latest version.

# Install the aks-preview extension
az extension add --name aks-preview
 
# Update the extension to make sure you have the latest version installed
az extension update --name aks-preview

#Next you will need to register the extension. To do that you can use the following command.
az feature register --namespace "Microsoft.ContainerService" --name "StartStopPreview"
 
#After a few minutes you will need to check to see if the status of this feature is registered. To do that run the following. If it still shows Registering just wait a few minutes longer.
az feature list -o table --query "[?contains(name, 'Microsoft.ContainerService/StartStopPreview')].{Name:name,State:properties.state}"

#When the feature is registered, refresh the registration for the Microsoft.ContainerService resource provider with this command:
az provider register --namespace Microsoft.ContainerService

#You can stop a cluster with this command, where you fill in the name of your AKS cluster and the resource group of the cluster:
az aks stop --name myAKSCluster --resource-group myResourceGroup
az aks stop --name $(terraform output -raw kubernetes_cluster_name) --resource-group $(terraform output -raw resource_group_name)

#When the cluster is stopped, you can check to see if it is, by running the following command:
az aks show --name myAKSCluster --resource-group myResourceGroup
az aks show --name $(terraform output -raw kubernetes_cluster_name) --resource-group $(terraform output -raw resource_group_name)

#In the JSON that follows, you can see the status of the cluster by looking at the powerState, which will show Stopped.

#You can easily start the cluster again with the this command:
az aks start --name myAKSCluster --resource-group myResourceGroup
az aks start --name $(terraform output -raw kubernetes_cluster_name) --resource-group $(terraform output -raw resource_group_name)


References: 
https://pixelrobots.co.uk/2020/09/the-official-way-to-stop-and-start-your-azure-kubernetes-service-aks-cluster/
https://docs.microsoft.com/en-us/azure/aks/start-stop-cluster

